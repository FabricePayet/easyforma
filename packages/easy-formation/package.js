Package.describe({
  name: 'easy-formation',
});

Package.onUse(function (api) {

  api.use([
    'vulcan:core@1.12.9',
    'vulcan:forms@1.12.9',
    'vulcan:accounts@1.12.9',
    'vulcan:ui-bootstrap@1.12.9',
    'fourseven:scss@4.5.0',
  ]);

  api.addFiles('lib/stylesheets/style.scss');

  api.addAssets([
    'lib/static/logo.png'
  ], ['client']);

  api.mainModule('lib/server/main.js', 'server');
  api.mainModule('lib/client/main.js', 'client');

});
