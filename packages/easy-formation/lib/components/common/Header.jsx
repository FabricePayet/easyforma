import React from 'react';
import { registerComponent, Components, withCurrentUser } from 'meteor/vulcan:core';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import Users from 'meteor/vulcan:users';
import { Link } from 'react-router';

const NavLoggedIn = ({ currentUser }) => (
  <div className="header-nav header-logged-in">
    <div className="header-accounts">
      Bienvenue,&nbsp;
      <Components.ModalTrigger label={Users.getDisplayName(currentUser)} size="small">
        <div>
          {Users.isAdmin(currentUser) ? <p>Administrateur</p> : null}
          <Components.AccountsLoginForm />
        </div>
      </Components.ModalTrigger>
    </div>
  </div>
);

// navigation bar component when the user is logged out
const NavLoggedOut = ({ currentUser }) =>
  <div className="header-nav header-logged-out">
    <Components.ModalTrigger label="Sign Up/Log In" size="small">
      <Components.AccountsLoginForm />
    </Components.ModalTrigger>
  </div>

// Header component

const Header = ({ currentUser }) =>
  <Navbar style={{ backgroundColor: '#ddd' }}>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to="/">
          <img src="/packages/easy-formation/lib/static/logo.png" alt="EasyForma: mon centre de formation connecté" />
        </Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Nav style={{ display: 'flex' }}>
      <NavItem eventKey={1} href="/formateurs" style={{ marginRight: '24px' }}>
        Mes formateurs
      </NavItem>
      <NavItem eventKey={2} href="/">
        Mes promos
      </NavItem>
    </Nav>

    <Navbar.Collapse>
      {
        currentUser ?
          <NavLoggedIn currentUser={currentUser} /> :
          <NavLoggedOut currentUser={currentUser} />
      }
      {/* <Navbar.Text pullRight>
        <Navbar.Text>
        </Navbar.Text>
      </Navbar.Text> */}
    </Navbar.Collapse>
  </Navbar>

registerComponent({ name: 'Header', component: Header, hocs: [withCurrentUser] });
