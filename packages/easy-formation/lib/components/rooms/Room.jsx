import React, { PropTypes, Component } from 'react';
import { Components, registerComponent, withSingle, withCurrentUser, Loading } from "meteor/vulcan:core";

import Promos from '../../modules/promos/collection';

function Room({ params }) {
  return (
    <div style={{ maxWidth: '500px', margin: '20px auto' }}>
      {/* <div style={{ marginBottom: '24px' }}>
        <Components.AccountsLoginForm />
      </div> */}
      <Components.RoomInner documentId={params.promoId} />
    </div>
  );
}

registerComponent({ name: 'Room', component: Room });

const RoomInner = ({ document, loading }) =>
  <div>
    {
      loading ? <Loading /> :
        <div>
          <div><a href="/">Retour</a></div>
          <h1>{document.name} ({document.year})</h1>
          <div>
            <h4>Formateurs</h4>
            <ul>
              {document.formateurs.map(formateur => <li>{formateur.firstname} {formateur.lastname}</li>)}
            </ul>
            <Components.Button>Ajouter un formateur</Components.Button>
          </div>
          <hr />
          <div>
            <h4>Apprenant</h4>
            <p>Pas encore d'inscrit</p>
            <Components.Button>Ajouter un éleve</Components.Button>
          </div>
        </div>
    }
  </div>

const options = {
  collection: Promos,
  fragmentName: 'PromosRoomFragment'
};

registerComponent({ name: 'RoomInner', component: RoomInner, hocs: [[withSingle, options], withCurrentUser] });
