import React, { PropTypes, Component } from 'react';
import { registerComponent, Components } from 'meteor/vulcan:core';

const Login = () =>

  <div style={{ maxWidth: '500px', margin: '20px auto' }}>
    Se connecter

    <div>
      <Components.AccountsLoginForm />
    </div>

    <div className="login">

      {/* new document form placeholder */}

      {/* documents list placeholder */}

      {/* load more placeholder */}

    </div>

  </div>

registerComponent({ name: 'Login', component: Login });
