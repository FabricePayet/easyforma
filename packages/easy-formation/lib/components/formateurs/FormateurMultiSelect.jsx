import React, { PropTypes, Component } from 'react';
import { registerComponent, Components } from 'meteor/vulcan:core';
import Multiselect from 'react-bootstrap-multiselect';

const FormateurMultiSelect = ({ formateur }) =>
  <div>
    <Multiselect data={[{ value: 'One', selected: true }, { value: 'Two' }]} />
  </div>


registerComponent({ name: 'FormateurMultiSelect', component: FormateurMultiSelect });
