import React, { PropTypes, Component } from 'react';
import { Components, registerComponent, withMulti, withCurrentUser, Loading } from 'meteor/vulcan:core';

import Promos from '../../modules/promos/collection.js';
import Formateurs from '../../modules/formateurs/collection.js';

const PromosList = ({ results = [], currentUser, loading, loadMore, count, totalCount }) =>

  <div style={{ maxWidth: '500px', margin: '20px auto' }}>
    {loading ?

      <Loading /> :

      <div className="promos">
        {
          Promos.options.mutations.create.check(currentUser) ?
            <Components.ModalTrigger label="Ajouter une promotion">
              <Components.PromosNewForm />
            </Components.ModalTrigger>
            : null
        }
        <hr />
        {results.map(promo => <Components.PromosItem key={promo._id} promo={promo} currentUser={currentUser} />)}

        {totalCount > results.length ?
          <a href="#" onClick={e => { e.preventDefault(); loadMore(); }}>Charger plus ({count}/{totalCount})</a> : ''
        }
      </div>
    }

  </div>

const options = {
  collection: Promos,
  fragmentName: 'PromosRoomFragment',
  limit: 5
};

registerComponent({ name: 'PromosList', component: PromosList, hocs: [[withMulti, options], withCurrentUser] });
