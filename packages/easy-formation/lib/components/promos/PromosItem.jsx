import React, { PropTypes, Component } from 'react';
import { registerComponent, Components } from 'meteor/vulcan:core';
import { Link } from 'react-router';

import Promos from '../../modules/promos/collection.js';

const PromosItem = ({ promo, currentUser }) =>

  <div style={{ paddingBottom: "15px", marginBottom: "15px", borderBottom: "1px solid #ccc" }}>
    <h4><Link to={`/rooms/${promo._id}`}>{promo.name} ({promo.year})</Link></h4>
    <p>Académie des savoirs - Saint Pierre</p>
    {
      promo.formateurs.length ?
        <div>
          {promo.formateurs.length} formateurs
      </div> :
        null
    }

    {
      Promos.options.mutations.update.check(currentUser, promo) ?
        <Components.ModalTrigger label="Editer la promotion">
          <Components.PromosEditForm currentUser={currentUser} documentId={promo._id} />
        </Components.ModalTrigger>
        : null
    }
  </div>

registerComponent({ name: 'PromosItem', component: PromosItem });
