import React, { PropTypes, Component } from 'react';
import { Components, registerComponent, withList, getFragment, withCurrentUser } from 'meteor/vulcan:core';

import Promos from '../../modules/promos/collection.js';

const PromosNewForm = ({ currentUser, closeModal }) =>

  <div>
    {
      Promos.options.mutations.create.check(currentUser) ?
        <div style={{ marginBottom: '20px', paddingBottom: '20px', borderBottom: '1px solid #ccc' }}>
          <h4>Ajouter une promotion</h4>
          <Components.SmartForm
            collection={Promos}
            mutationFragment={getFragment('PromosItemFragment')}
            successCallback={document => {
              closeModal();
            }}
          />
        </div> :
        null
    }
  </div>

registerComponent({ name: 'PromosNewForm', component: PromosNewForm, hocs: [withCurrentUser] });
