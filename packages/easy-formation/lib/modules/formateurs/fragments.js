import { registerFragment } from 'meteor/vulcan:core';

registerFragment(`
  fragment FormateursItemFragment on Formateur {
    _id
    createdAt
    userId
    user {
      displayName
    }
    firstname
    lastname
    description
  }
`);

registerFragment(`
  fragment FormateursList on Formateur {
    _id
    firstname
    lastname
  }
`)
