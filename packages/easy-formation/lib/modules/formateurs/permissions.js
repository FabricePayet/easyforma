import Users from 'meteor/vulcan:users';

const membersActions = [
  'formateur.create',
  'formateur.update',
  'formateur.delete',
];

const guestsActions = [
];

Users.groups.members.can(membersActions);

Users.groups.guests.can(guestsActions);
