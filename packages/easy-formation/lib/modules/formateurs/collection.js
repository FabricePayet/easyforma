import { createCollection, getDefaultResolvers, getDefaultMutations } from 'meteor/vulcan:core';
import schema from './schema.js';
import './fragments.js';
import './permissions.js';

const Formateurs = createCollection({

  collectionName: 'Formateurs',

  typeName: 'Formateur',

  schema: schema,

  resolvers: getDefaultResolvers({ typeName: 'Formateur' }),

  mutations: getDefaultMutations({ typeName: 'Formateur' }),

});

export default Formateurs;
