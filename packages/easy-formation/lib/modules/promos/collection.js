import { createCollection, getDefaultResolvers, getDefaultMutations } from 'meteor/vulcan:core';
import schema from './schema.js';
import './fragments.js';
import './permissions.js';

const Promos = createCollection({

  collectionName: 'Promos',

  typeName: 'Promo',

  schema: schema,

  resolvers: getDefaultResolvers({ typeName: 'Promo' }),

  mutations: getDefaultMutations({ typeName: 'Promo' }),

});

export default Promos;
