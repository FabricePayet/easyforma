import Formateurs from '../../modules/formateurs/collection';

const schema = {
  _id: {
    type: String,
    optional: true,
    canRead: ['guests'],
  },
  createdAt: {
    type: Date,
    optional: true,
    canRead: ['guests'],
    onCreate: () => {
      return new Date();
    },
  },
  userId: {
    type: String,
    optional: true,
    canRead: ['guests'],
    resolveAs: {
      fieldName: 'user',
      type: 'User',
      resolver: (movie, args, context) => {
        return context.Users.findOne({ _id: movie.userId }, { fields: context.Users.getViewableFields(context.currentUser, context.Users) });
      },
      addOriginalField: true
    }
  },

  // custom properties
  name: {
    label: 'Nom',
    type: String,
    optional: true,
    canRead: ['guests'],
    canCreate: ['members'],
    canUpdate: ['members'],
  },
  year: {
    label: 'Année',
    type: Number,
    optional: true,
    canRead: ['guests'],
    canCreate: ['members'],
    canUpdate: ['members'],
  },

  formateursIds: {
    type: Array,
    optional: true,
    canRead: ['guests'],
    canCreate: ['members'],
    canUpdate: ['members'],
    label: 'Formateurs',
    input: 'checkboxgroup',
    options: (props) => {
      return props.data.formateurs.results.map(formateur => ({
        label: `${formateur.firstname} ${formateur.lastname}`,
        value: formateur._id
      }))
    },
    resolveAs: {
      fieldName: 'formateurs',
      type: '[Formateur]',
      resolver: (document, args, context) => {
        if (!document.formateursIds) { return [] }
        return document.formateursIds.map(formateurId => context.Formateurs.findOne(formateurId));
      },
      // addOriginalField: true
    },
    query: `
      formateurs {
        results{
          _id
          firstname
          lastname
        }
      }
    `,
  },

  'formateursIds.$': {
    type: String,
    optional: true,
    canRead: ['guests']
  },


};

export default schema;
