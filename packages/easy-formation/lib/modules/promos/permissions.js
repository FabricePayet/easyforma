import Users from 'meteor/vulcan:users';

const membersActions = [
  'promo.create',
  'promo.update',
  'promo.delete',
];

const guestsActions = [
];

Users.groups.members.can(membersActions);

Users.groups.guests.can(guestsActions);
